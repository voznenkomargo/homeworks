"use strict";
const images = document.querySelectorAll(".image-to-show");
const btnStop = document.querySelector(".btn-stop");
const btnStart = document.querySelector(".btn-start");


let n = 1;
let interval = 0;



function changeImg() {
   images.forEach(elem => elem.classList.remove("active"))
   images[n].classList.add("active");
   n++;
   if (n === images.length) {
      n = 0;
   }
}

function start() {
   if (interval == 0) {
      interval = setInterval(changeImg, 3000);
   }
}


function stop() {
   clearInterval(interval);
   interval = 0;
}

window.onload = function () {
   start()
};
btnStop.onclick = function () {
   stop()
};
btnStart.onclick = function () {
   start()
};