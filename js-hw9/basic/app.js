"use strict";
const tabs = document.querySelectorAll(".tabs-title");
const liText = document.querySelectorAll(".tabs-content li");


tabs.forEach(function (tab) {
   tab.addEventListener("click", () => {
      let currentTab = tab;
      let tabCategory = currentTab.getAttribute("data-tab");
      let currentCat = document.querySelector(tabCategory);
      if (!currentTab.classList.contains("active")) {
         tabs.forEach(tab => tab.classList.remove("active"));
         liText.forEach(text => text.classList.remove("active"));
         currentTab.classList.add("active");
         currentCat.classList.add("active");
      }
   })
});