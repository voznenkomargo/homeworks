"use strict";



function addList(array, dom = document.body) {
   let newArray = array.map(elem => {
      let city = elem.split("-").toString().toUpperCase();
      let item = document.createElement('p');
      item.innerHTML = `город Украины  - <strong>${city}</strong>`;
      item.className = "items";
      dom.append(item);
   });
   return newArray;

};

addList(["Vinnitsa", "Uman", "Kiev", "Kharkiv", "Odessa", "Lviv"]);