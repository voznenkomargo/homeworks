$(document).ready(function () {
   $("a.nav-menu-link").click(function () {
      let elementClick = $(this).attr("href")
      let destination = $(elementClick).offset().top
      $("html:not(:animated),body:not(:animated)").animate({
         scrollTop: destination
      }, 800);
      return false;
   });
   let btn = $('#button');
   $(window).scroll(function () {
      if ($(window).scrollTop() > 600) {
         btn.show();
      } else {
         btn.hide();
      }
   });
   btn.on('click', function (e) {
      e.preventDefault();
      $('html, body').animate({
         scrollTop: 0
      }, '800');
   });

   $(".slide-toggle").click(function () {
      $(".clients").slideToggle();

   });

});