"use strict";
let userNumber;
do {
   userNumber = prompt("Enter your number", "");
} while (!userNumber || userNumber.trim() === "" || isNaN(+userNumber) || !Number.isInteger(+userNumber)); // продвинутая часть - определение на целое число и проверки на ввод 



if (userNumber < 5) {
   console.log("Sorry, no numbers")
} else {
   for (let i = 0; i <= userNumber; i++) {
      if (i % 5 === 0) {
         console.log(i);
      }
   }
}