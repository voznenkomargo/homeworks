"use strict";

const btnChange = document.querySelector(".change-color");

const block = document.querySelector(".main-block");
const header = document.querySelector(".header-nav");


function switchThemeColor() {
   document.body.classList.toggle("dark");
   block.classList.toggle("dark");
   header.classList.toggle("dark");

   if (localStorage.getItem('dark') === 'dark') {
      localStorage.removeItem("dark");
   } else {
      localStorage.setItem("dark", "dark");
   }

};
window.onload = function () {
   if (localStorage.getItem('dark') === 'dark') {
      document.body.classList.add("dark");
      block.classList.add("dark");
      header.classList.add("dark");

   }
}

btnChange.addEventListener('click', switchThemeColor);