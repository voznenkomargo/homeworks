class Employee {
   /**
    * 
    * @param {string} name 
    * @param {number} age 
    * @param {number} salary 
    */
   constructor(name, age, salary) {
      this._name = name;
      this._age = age;
      this._salary = salary;
   }

   get name() {
      return this._name;
   }
   get age() {
      return this._age;
   }
   get salary() {
      return this._salary;
   }
   set name(name) {
      this._name = name;
   }
   set age(age) {
      this._age = age;
   }
   set salary(salary) {
      this._salary = salary;
   }
}

class Programmer extends Employee {
   /**
    * @param {string} name 
    * @param {number} age 
    * @param {number} salary 
    * @param {string} lang 
    */
   constructor(name, age, salary, lang) {
      super(name, age, salary);
      this._lang = lang;
   }
   get salary() {
      return this._salary * 3;
   }
}
const alex = new Employee("Алекс", 20, 5000);
console.log(alex);
console.log(alex.name);
console.log(alex.age);
console.log(alex.salary);

const developer = new Programmer("Алекс", 18, 4000, "русский");
console.log(developer);
console.log(developer.name);
console.log(developer.age);
console.log(developer.salary);

const android = new Programmer("Макс", 17, 9000, "русский");
console.log(android);