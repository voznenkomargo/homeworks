const books = [{
      author: "Скотт Бэккер",
      name: "Тьма, что приходит прежде",
      price: 70
   },
   {
      author: "Скотт Бэккер",
      name: "Воин-пророк",
   },
   {
      name: "Тысячекратная мысль",
      price: 70
   },
   {
      author: "Скотт Бэккер",
      name: "Нечестивый Консульт",
      price: 70
   },
   {
      author: "Дарья Донцова",
      name: "Детектив на диете",
      price: 40
   },
   {
      author: "Дарья Донцова",
      name: "Дед Снегур и Морозочка",
   }
];




function addList(array, selector) {
   const ul = document.createElement('ul');
   let newArray = array.map(elem => {
      try {
         if (!elem.author) {
            throw new Error("Не указан автор");
         }
         if (!elem.name) {
            throw new Error("Не указано название");
         }
         if (!elem.price) {
            throw new Error("Не указана цена");
         }
         let item = document.createElement('li');
         item.innerHTML = `${elem.author}, ${elem.name}, ${elem.price}`;
         item.className = "item";
         ul.append(item);
         document.querySelector(selector).append(ul);

      } catch (err) {
         console.log(err);
      }

   })
}


addList(books, "#root");