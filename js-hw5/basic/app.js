"use strict";


function createNewUser() {
   let firstName = prompt("Enter your name");
   let lastName = prompt("Enter your surname");
   let birthday = prompt("When's your birthday?", "dd.mm.yyyy").split('.');
   let birthDate = new Date(birthday[2], birthday[1], birthday[0]);
   let today = new Date();

   let newUser = {
      birthDate: birthDate,
      firstName: firstName,
      lastName: lastName,
      birthDate: birthDate,

      getLogin: function () {
         return (this.firstName[0] + this.lastName).toLowerCase();
      },
      getAge: function () {

         const userAgeDifference = Math.floor((Date.now() - birthDate.getTime()) / 1000 / (60 * 60 * 24) / 365.2425);

         return userAgeDifference;
      },
      getPassword: function () {
         return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthDate.getFullYear();
      }
   };
   return newUser;
};
const user = createNewUser();
console.log(user.getAge());
console.log(user.getPassword());