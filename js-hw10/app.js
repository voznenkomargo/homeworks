"use strict";

const iconShow = document.querySelectorAll(".fa-eye");
const submitBtn = document.querySelector(".btn");
const enterPass = document.querySelector(".enterPass");
const submitPass = document.querySelector(".submitPass");
const form = document.querySelector(".password-form");
const wrapperSecond = document.querySelector(".second");





iconShow.forEach(item => {
   item.addEventListener("click", () => {
      item.classList.toggle("fa-eye");
      item.classList.toggle("fa-eye-slash");
      if (item.classList.contains("fa-eye")) {
         item.previousElementSibling.setAttribute("type", "password");
      }
      if (item.classList.contains("fa-eye-slash")) {
         item.previousElementSibling.setAttribute("type", "text");
      }

   })
})


form.addEventListener("submit", function (evt) {
   evt.preventDefault();
   let alarmText;
   if (enterPass.value === submitPass.value) {
      alarmText = document.querySelector(".alarm");
      if (alarmText) {
         alarmText.textContent = "";
      }

      alert("You are welcome");
      enterPass.value = '';
      submitPass.value = '';

   }

   if (enterPass.value !== submitPass.value) {

      let alarmTextspan = document.querySelector(".alarm");



      if (!alarmTextspan || alarmTextspan.textContent != "Нужно ввести одинаковые значения") {
         alarmTextspan = document.createElement("span");
         alarmTextspan.textContent = "Нужно ввести одинаковые значения";
         alarmTextspan.classList.add("alarm");
         wrapperSecond.after(alarmTextspan);
         submitBtn.disabled = true;

         enterPass.addEventListener("focus", () => {
               submitBtn.disabled = false;
            }),
            submitPass.addEventListener("focus", () => {
               submitBtn.disabled = false;
            })
      }
   }



})