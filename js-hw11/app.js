"use strict";

let btn = Array.from(document.querySelectorAll(".btn"));

function btnDown(letter) {
   let button = btn.filter(item => item.textContent === letter);
   button.forEach(el => el.style.backgroundColor = "blue");
   let arr = btn.filter(item => item.textContent !== letter);
   arr.forEach(el => el.style.backgroundColor = "black");
}
document.addEventListener("keydown", (event) => {
   if (event.code === "Enter") {
      btnDown("Enter");
   };
   if (event.code === "KeyS") {
      btnDown("S");
   };
   if (event.code === "KeyE") {
      btnDown("E");
   };
   if (event.code === "KeyO") {
      btnDown("O");
   };
   if (event.code === "KeyN") {
      btnDown("N");
   };
   if (event.code === "KeyL") {
      btnDown("L");
   };
   if (event.code === "KeyZ") {
      btnDown("Z");
   };
})