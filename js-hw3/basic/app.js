"use strict";

let num1 = +prompt("Enter number 1!");
let num2 = +prompt("Enter number 2!");
let oper = prompt("Enter math operation");

function getResult(num1, num2, oper) {
   switch (oper) {
      case "/":
         return num1 / num2;
      case "+":
         return num1 + num2;
      case "-":
         return num1 - num2;
      case "*":
         return num1 * num2;
      default:
         return;
   }
}
console.log(getResult(num1, num2, oper));