"use strict";


function filterBy(array, type) {
   let newArray = array.filter((elem => typeof elem !== type));
   return newArray;

};
//проверка
console.log(filterBy(['hello', 'world', 23, '23', null, undefined, true, {
   a: 58
}], 'object')); // массив кроме обьектов  (в том числе null)
console.log(filterBy(['hello', 'world', 23, '23', null, undefined, true, {
   a: 58
}], 'string')); // массив кроме строк
console.log(filterBy(['hello', 'world', 23, '23', null, undefined, true, {
   a: 58
}], 'number')); // массив кроме чисел
console.log(filterBy(['hello', 'world', 23, '23', null, undefined, true, {
   a: 58
}], 'boolean')); // массив кроме boolean
console.log(filterBy(['hello', 'world', 23, '23', null, undefined, true, {
   a: 58
}], 'undefined')); // массив кроме undefined