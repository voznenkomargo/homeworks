"use strict";

const input = document.querySelector(".price");
const inputField = document.querySelector(".inputField");
const span = document.createElement("span");
const closeBtn = document.createElement("div");
const spanfield = document.querySelector(".spanfield");



input.addEventListener("focus", () => {
   input.style.border = "2px solid green";


})
input.addEventListener("blur", () => {
   let inputValue = +input.value;
   console.log(typeof inputValue);
   if (inputValue > 0) {
      input.style.border = "2px solid white";
      span.innerHTML = `Текущая цена: ${inputValue}`;
      spanfield.append(span);
      input.style.color = "green";
      closeBtn.classList.add("closebtn");
      spanfield.append(closeBtn);

   } else {
      input.style.border = "2px solid red";
      span.textContent = "Please enter correct price";
      inputField.after(span);
   }
})

closeBtn.addEventListener("click", () => {
   span.remove();
   closeBtn.remove();
   input.value = '';
})